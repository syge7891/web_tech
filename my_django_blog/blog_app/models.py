from django.db import models
from django.db.models.manager import Manager

# Create your models here.


class Article(models.Model):
    objects = Manager()
    article_title = models.CharField('Название статьи', max_length=300)
    article_text = models.TextField('Текст статьи')
    article_image = models.ImageField(upload_to='media/')
    pub_date = models.DateTimeField('Дата публикации')
    article_post_description = models.TextField('Описание', max_length=500)

    def __str__(self):
        return self.article_title

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'


class Comment(models.Model):
    objects = Manager()
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('Имя автора', max_length=50)
    comment_text = models.CharField('Текст комментария', max_length=250)

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
