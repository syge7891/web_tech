from django.shortcuts import render
from blog_app.models import Article, Comment
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.


def home(request):
    all_articles_list = Article.objects.all()
    return render(request, "home.html", {'all_articles_list': all_articles_list})


def list(request, article_id):
    a = Article.objects.get(id=article_id)
    latest_comments_list = a.comment_set.order_by('-id')[:10]
    return render(request, "list.html", {'a': a, 'latest_comments_list': latest_comments_list})


def leave_comment(request, article_id):
    a = Article.objects.get(id=article_id)
    a.comment_set.create(
        author_name=request.POST['name'], comment_text=request.POST['text'])
    return HttpResponseRedirect(reverse('list', args=(a.id,)))
